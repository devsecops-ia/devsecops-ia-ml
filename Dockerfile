
# INSTALA LA VERSIÓN DE R NECESARIA
FROM rocker/r-ver:4.0.2


# INSTALA LAS LIBRERÍAS DEL SO NECESARIAS

RUN apt-get update -qq && apt-get install -y \
libssl-dev \
libcurl4-gnutls-dev 
#zlib1g-dev \
#libxml2-dev \
#libsodium-dev

# Instalar paquetes necesarios para Poder levantar la API
### Los paquetes que deben figurar aquí deben ser los mismos que aparecen en los archivos de R (main.R, API.R)

RUN R -e 'install.packages("plumber")' && \
R -e 'install.packages("xgboost")' && \
R -e 'install.packages("futile.logger")' && \
R -e 'install.packages("digest")' && \
R -e 'install.packages("caret")' && \
R -e 'install.packages("tictoc")' 

# SE COPIA EL DIRECTORIO CON LOS ARCHIVOS DEL REPO EN EL DIRECTORIO PRINCIPAL DEL CONTENEDOR

COPY / /
	
# SE EXPONE EL PUERTO EN EL QUE CORRE LA API

EXPOSE 8080

# SE EJECUTA EL SCRIPT main.R QUE LEVANTA LA API

CMD ["Rscript", "main.R", "8080"]
