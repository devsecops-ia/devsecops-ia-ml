## LIBRARY

library(caret)
library(ggplot2)
library(GGally)
library(rpart)
library(gbm)      # <------ standard gradient boosting 
library(xgboost)  # <------ XGBOOST

### LOAD DATA

df<- read.csv("Dataset.csv")
df<-df[,c(2,3,4,5)] ## Eliminamos la columna repo identifier
df<-df[df$story_point %in% c(1,2,3,5,8,13,21,34),] ## Filtramos
df$story_point <- (df$story_point)
colnames(df) <- c("Y","X1","X2","X3")
df[, c(2,3,4)] <- sapply(df[, c(2,3,4)], as.numeric)
df$Y=make.names(df$Y)

str(df)


### DATA PARTITION

seedTRTVpartition = 200
set.seed(seedTRTVpartition) #For replication
ratioTR = 0.8 #Percentage for training
#create random 80/20 % split
trainIndex <- createDataPartition(df$Y,      #output variable. createDataPartition creates proportional partitions
                                  p = ratioTR,  #split probability
                                  list = FALSE, #Avoid output as a list
                                  times = 1)    #only one partition
#obtain training and validation sets
fTR = df[trainIndex,]
fTV = df[-trainIndex,]



### Control tune
ctrl <- trainControl(method = "none",                      # method= "none" when no resampling is used
                     summaryFunction = defaultSummary,    #Performance summary for comparing models in hold-out samples
                     classProbs = TRUE,                    #Compute class probs in Hold-out samples
                     returnResamp = "all",                 #Return all information about resampling
                     savePredictions = TRUE) 
# with cross-validation
ctrl_tune <- trainControl(method = "cv",                        #k-fold cross-validation
                          number = 5,                          #Number of folds
                          summaryFunction = defaultSummary,    #Performance summary for comparing models in hold-out samples
                          classProbs = TRUE,                    #Compute class probs in Hold-out samples
                          returnResamp = "all",                 #Return all information about resampling
                          savePredictions = TRUE)               #Compute class probs in Hold-out samples

### XBOOST MODEL

set.seed(150) #For replication
#Parameter tuning
xgbGrid <- expand.grid(
  nrounds = 150, #Boosting Iterations
  eta = 0.3,     #Shrinkage
  max_depth = 2, #Max Tree Depth
  gamma = 0,    #Minimum Loss Reduction
  colsample_bytree=1, #Subsample Ratio of Columns
  min_child_weight=1, #Minimum Sum of Instance Weight
  subsample = 0.5    #Subsample Percentage
)
# Train the GBM
set.seed(150)
xGBM.fit =  train(fTR[,c("X1","X2","X3")], #Input variables. Other option: fdata[,1:2]
                  y = fTR$Y,
                  method = "xgbTree",
                  #tuneGrid = xgbGrid, #Cuando no se que grid escoger me lo escoge caret 
                  tuneLength = 4, #Use caret tuning
                  trControl = ctrl_tune,    #use cv
                  metric = "accuracy",
                  verbose = FALSE) # The xgbTree() function produces copious amounts of output, avoid printing a lot



#plot grid
# helper function for the plots
tuneplot <- function(x, probs = .90) {
  ggplot(x) + coord_cartesian(ylim = c(quantile(x$results$RMSE, probs = probs), min(x$results$RMSE))) + theme_bw()
}
tuneplot(xGBM.fit)

#Measure for variable importance
varImp(xGBM.fit,scale = FALSE)
plot(varImp(xGBM.fit,scale = FALSE))

tree.fit = xGBM.fit;
## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and validation sets
#training
fTR_eval = fTR
fTR_eval$tree_prob = predict(tree.fit, type="prob" , newdata = fTR) # predict probabilities
fTR_eval$tree_pred = predict(tree.fit, type="raw" , newdata = fTR) # predict classes 
#validation
fTV_eval = fTV
fTV_eval$tree_prob = predict(tree.fit, type="prob" , newdata = fTV) # predict probabilities
fTV_eval$tree_pred = predict(tree.fit, type="raw" , newdata = fTV) # predict classes 

#Plot classification in a 2 dimensional space
Plot2DClass(fTR[,1:6], #Input variables of the model
            fTR$Y,     #Output variable
            tree.fit,#Fitted model with caret
            var1="X1", var2="X2", #variables that define x and y axis
            selClass = "INS")     #Class output to be analyzed 

## Performance measures: confusion matices
fTR_eval
str(fTR_eval)

fTR_eval$Y = as.factor(fTR_eval$Y)
fTV_eval$Y = as.factor(fTV_eval$Y)

confusionMatrix(fTR_eval$tree_pred, fTR_eval$Y) # Training
confusionMatrix(fTV_eval$tree_pred,  fTV_eval$Y)# Validation



### CLASIFICANDO EN TRES CLASES
df$Y=as.character(df$Y)
df$agrupado=df$Y
df$agrupado[df$Y %in% c("X1","X2","X3")]<- "small"
df$agrupado[df$Y %in% c("X5","X8")]<- "medium"
df$agrupado[df$Y %in% c("X13","X21")]<- "big"
df$agrupado=as.factor(df$agrupado)
df$Y=as.factor(df$Y)

str(df)

library(dplyr)
library(ROSE)
df %>% 
  count(agrupado)

library(rpart)


## SE APLICA UNDERSAMPLING
## rows that have "z" and "zz" entries
s_ind <- which(df$agrupado == "small")
m_ind <- which(df$agrupado == "medium")
b_ind <- which(df$agrupado == "big")

nsamp <- 123   #number of elements to sample
## if you want all elements of the smaller class, could be:
## nsamp <- min(length(z_ind), length(zz_ind))

## select `nsamp` entries with "z" and `nsamp` entries with "zz"
pick_s <- sample(s_ind, nsamp)
pick_m <- sample(m_ind, nsamp)
pick_b <- sample(b_ind, nsamp)

new_data <- df[c(pick_s, pick_m,pick_b), ]

## DIVISION DE LOS DATOS

fTR = new_data[trainIndex,]
fTV = new_data[-trainIndex,]

### XBOOST MODEL

set.seed(150) #For replication
#Parameter tuning
xgbGrid <- expand.grid(
  nrounds = 50, #Boosting Iterations
  eta = 0.3,     #Shrinkage
  max_depth = 1, #Max Tree Depth
  gamma = 0,    #Minimum Loss Reduction
  colsample_bytree=1, #Subsample Ratio of Columns
  min_child_weight=1, #Minimum Sum of Instance Weight
  subsample = 0.5    #Subsample Percentage
)
# Train the GBM
set.seed(150)
xGBM.fit =  train(fTR[,c("X2","X3")], #Input variables. Other option: fdata[,1:2]
                  y = fTR$agrupado,
                  method = "xgbTree",
                  tuneGrid = xgbGrid, #Cuando no se que grid escoger me lo escoge caret 
                  tuneLength = 4, #Use caret tuning
                  trControl = ctrl_tune,    #use cv
                  metric = "Accuracy",
                  verbose = FALSE) # The xgbTree() function produces copious amounts of output, avoid printing a lot



#plot grid
# helper function for the plots
tuneplot <- function(x, probs = .90) {
  ggplot(x) + coord_cartesian(ylim = c(quantile(x$results$RMSE, probs = probs), min(x$results$RMSE))) + theme_bw()
}
tuneplot(xGBM.fit)

#Measure for variable importance
varImp(xGBM.fit,scale = FALSE)
plot(varImp(xGBM.fit,scale = FALSE))

tree.fit = xGBM.fit;
## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and validation sets
#training
fTR_eval = fTR
fTR_eval$tree_prob = predict(tree.fit, type="prob" , newdata = fTR) # predict probabilities
fTR_eval$tree_pred = predict(tree.fit, type="raw" , newdata = fTR) # predict classes 
#validation
fTV_eval = fTV
fTV_eval$tree_prob = predict(tree.fit, type="prob" , newdata = fTV) # predict probabilities
fTV_eval$tree_pred = predict(tree.fit, type="raw" , newdata = fTV) # predict classes 


## Performance measures: confusion matices


fTR_eval$Y = as.factor(fTR_eval$Y)
fTV_eval$Y = as.factor(fTV_eval$Y)

confusionMatrix(fTR_eval$tree_pred, fTR_eval$agrupado) # Training
confusionMatrix(fTV_eval$tree_pred,  fTV_eval$agrupado)# Validation




### SE GUARDA EL MODELO EN RDS



saveRDS(xGBM.fit, "model.rds")
