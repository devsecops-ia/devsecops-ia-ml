library(plumber)
library(xgboost)
library(futile.logger)
library(digest)
library(caret)

#* @apiTitle API xgb
#* @apiDescription Esto es una API para un modelo xgboost en R

modelo <-readRDS('model.rds')


#* Chequear si funciona la API
#* @get /check

estado <- function(){
  list(
    estado = 'todo bien',
    hora = Sys.time()
  )
}



#* Modelo XGB
#* @param dt:list
#* @post /predict




function(dt,res) {

  
  df<-data.frame(as.list(dt))


  out <- predict(modelo, df, type = 'raw')

  as.character(out)
}



